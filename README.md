# validation-upload
<p align="center"><a href="#" target="_blank"><img src="https://ebphtb.mitraprimautama.id/storage/public/dist/img/1bUZMmUnQODxCY8gY5rLVgzEPF9n1OwI1AilBQC0.png" height="400"></a></p>

SECURITY VALIDATION UPLOAD FOR MPU APP 

## Getting started

Penjelasan Module / Trait ini adalah berfungsi validasi upload apakah file yang diupload memiliki kriteria berikut : 
- Tipe file (pdf,img,jpg, jpeg).
- Maksimal size upload sesuai settingan.
- Validasi file gambar apakah isinya memiliki script yang mencurigakan atau tidak

Dibuat Untuk Aplikasi MPU Zend Framework 2

## Add your files
Tambahkan Codingan ini sebelum move file hasil upload untuk validasi imagenya. sesuaikan nama variable sesuai kebutuhan pada variabel $post['s_logofile']
```
    /**
        codingan sebelum upload
    **/

    if ($post['s_logofile']['name'] != null) {
        $upload = (new ValidationHelper)->validateAndUploadFile($post['s_logofile']);
        if (!$upload['status']) {
            return $this->redirect()->toRoute('setting_pemda');
        }
    }

    /**
        codingan upload
    **/
```

## File 

- library / Trait dapat dilihat pada file  : ValidationHelper.php
- Contoh Penerapan Dapat Dilihat Pada File : settingpemda.php


## Authors 
UNDERTAKER KERAK BUMI ex.openproject.id

## License
Version 1.0