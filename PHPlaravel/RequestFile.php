<?php

namespace App\Http\Requests\Pemeriksaan;

use Illuminate\Foundation\Http\FormRequest;

class PemeriksaanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dipilih aja bos salah satu

        // ini kalo bentuk filenya hanya 1 
        return [
            't_dokumen' => 'required|max:5120|mimes:jpeg,jpeg,jpg,png,pdf'
        ];

        // ini kalo bentuk filenya lebih dari 1 / array
        return [
            't_dokumen.*' => 'required|max:5120|mimes:jpeg,jpeg,jpg,png,pdf'
        ];
    }
}
