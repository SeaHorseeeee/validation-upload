<?php 

    public function uploadsyaratpostAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();

        $req = $this->getRequest();
            $id = (int) $req->getQuery()->get('idspt');
            $idsyarat = (int) $req->getQuery()->get('idsyarat');
            if(!empty($id)){
                
                $datacari = $this->getTblSSPDBphtb()->getDataId_all($id);
                $letak_dir1="public";
                $letak_dir2="upload";
                $letak_dir3="filetransaksi";
                $letak_dir4=$datacari['t_periodespt'];
                $letak_dir5=str_replace(' ', '', strtolower($datacari['s_namajenistransaksi']));
                $letak_dir6=$datacari['t_idspt'];
                $letak_dir7=$req->getQuery()->get('idsyarat');

                $letak_dir = $letak_dir1.'/'.$letak_dir2.'/'.$letak_dir3.'/'.$letak_dir4.'/'.$letak_dir5.'/'.$letak_dir6.'/'.$letak_dir7.'/';
                $view = new ViewModel(array(
                    'get' => $req,
                    'letakfile' => $letak_dir,
                    'idspt' => $id,
                    'idsyarat' => $idsyarat,
                    'datasspd' => $datacari
                ));
                
                $options2 = array(
                    'delete_type' => 'POST',
                    
                );

                // file penting disini --------------
                // hanya validasi upload tanpa move file upload 
                $post = array_merge_recursive($req->getPost()->toArray(), $req->getFiles()->toArray());

                if (count($post['files']) != 0) {
                    foreach ($post['files'] as $key => $value) {
                        $upload = $this->validateAndUploadFile($value);
                        if (!$upload['status']) {
                            return $this->redirect()->refresh();
                        }
                    }
                }
                // end file penting disini --------------

                
                // upload seperti biasa
                $this->simpanfilepost($options2, $letak_dir, $id, $idsyarat, $datacari);
                
            }else{
                $view = new ViewModel(array(
                    'get' => $req,
                    
                ));
            }
        
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }