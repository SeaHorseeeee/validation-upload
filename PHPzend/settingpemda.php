<?php 
public function tambahAction() {
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Setting\PemdaFrm();
        $req = $this->getRequest();
        $newFile = "";
        if ($req->isPost()) {
            $base = new \Bphtb\Model\Setting\PemdaBase();
            $post = array_merge_recursive($req->getPost()->toArray(), $req->getFiles()->toArray());
            $frm->setData($post);
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());

                // file penting disini --------------
                // hanya validasi upload tanpa move file upload 
                if ($post['s_logofile']['name'] != null) {
                    $upload = (new ValidationHelper)->validateAndUploadFile($post['s_logofile']);
                    if (!$upload['status']) {
                        return $this->redirect()->refresh();
                    }
                }
                // end file penting disini --------------

                
                // upload seperti biasa
                $httpadapter = new \Zend\File\Transfer\Adapter\Http();
                $httpadapter->setDestination('public/upload/');
                if ($httpadapter->receive($post["s_logofile"]["name"])) {
                    $newFile = $httpadapter->getFileName();
                }
                
               
                $this->getTbl()->savedata($base, $newFile);
                return $this->redirect()->toRoute('setting_pemda');
            }
        }
    }
